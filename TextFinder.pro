#-------------------------------------------------
#
# Project created by QtCreator 2014-08-26T19:49:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TextFinder
TEMPLATE = app


SOURCES += main.cpp\
        textfinder.cpp

HEADERS  += textfinder.h

FORMS    += textfinder.ui

RESOURCES += \
    textfinder.qrc
